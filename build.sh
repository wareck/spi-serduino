#!/bin/bash
echo "installation des librairies"
sleep 1
apt-get update
apt-get install build-essential git avr-libc gcc-avr libpci-dev avrdude
echo "Decompression des fichiers"
sleep 2
if [ ! -d "arduino_firmware" ]; then
tar xvfz arduino_firmware.tar.gz
fi
if [ ! -d "flashrom-0.9.7" ]; then
tar xvfj flashrom-0.9.7.tar.bz2
fi
if [ ! -d "backup" ]; then
tar xvfJ backup.tar.xz
fi
echo "Compilation des programmes"
sleep 2
cd flashrom-0.9.7
if [ ! -e "flashrom" ]; then
make
fi
cd ..
cd arduino_firmware
if [ ! -e "serprog.hex" ]; then
make
fi
echo "Programmation de l'arduino"
echo "Press [ENTER] to continue...: "
read var_name
make upload
cd ..
echo "Arduino Ready..."
sleep 1
echo ""
echo "Test du firmware..."
ln -s flashrom-0.9.7/flashrom ./flashrom
./flashrom -p serprog:dev=/dev/ttyACM0:2000000
